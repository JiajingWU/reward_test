from django.http import HttpResponse
from django.shortcuts import render
from .reward_api import monitor, new_raise, new_lock, new_settlement


# Create your views here.

def check(req):
    monitor()
    return HttpResponse('monitor')


def raise_(req):
    try:
        status = new_raise()
    except:
        return HttpResponse('开启新合约错误')
    if status != 1:
        return HttpResponse('开启新合约失败')
    return HttpResponse('开启新合约成功,并向合约打入10cpc')


def lock(req):
    try:
        status = new_lock()
    except:
        return HttpResponse('进入锁定期错误')
    if status != 1:
        return HttpResponse('进入结算期失败')
    return HttpResponse('进入锁定期成功')


def settle(req):
    try:
        status = new_settlement()
    except Exception as e:
        return HttpResponse('进入结算期错误')
    print(status)
    if status != 1:
        return HttpResponse('进入结算期失败')
    return HttpResponse('进入结算期成功')
