import os
import sys
import time

from cpc_fusion import Web3
from solc import compile_files

sys.path.append('../../..')
os.chdir(sys.path[0])
from reward_test.config import cfg


def init():
    chain = cfg['chain']['ip']
    port = cfg['chain']['port']
    cf = Web3(Web3.HTTPProvider(f"http://{chain}:{port}"))
    owner = cf.toChecksumAddress("14ac5cf8f33696722386163be8a59e5f81e4157d")
    print('balance', cf.fromWei(cf.cpc.getBalance(owner), 'ether'))
    return cf, owner


cf, owner = init()
abi = [{'constant': True, 'inputs': [], 'name': 'settlementPeriod', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'round', 'outputs': [{'name': '', 'type': 'uint256'}], 'payable': False,
        'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'inLock', 'outputs': [{'name': '', 'type': 'bool'}], 'payable': False,
        'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [{'name': '', 'type': 'uint256'}], 'name': 'bonus',
        'outputs': [{'name': '', 'type': 'uint256'}], 'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'inSettlement', 'outputs': [{'name': '', 'type': 'bool'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'bonusPool', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'inRaise', 'outputs': [{'name': '', 'type': 'bool'}], 'payable': False,
        'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'nextSettlementTime', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'totalLockedBalance', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': 'amount', 'type': 'uint256'}], 'name': 'withdraw', 'outputs': [],
        'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'disable', 'outputs': [], 'payable': False,
        'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'nextLockTime', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'newLock', 'outputs': [], 'payable': False,
        'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'onlyNewSettlement', 'outputs': [], 'payable': False,
        'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'lockPeriod', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'raisePeriod', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': '_raisePeriod', 'type': 'uint256'}], 'name': 'setRaisePeriod',
        'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': True, 'inputs': [{'name': 'investor', 'type': 'address'}], 'name': 'totalBalanceOf',
        'outputs': [{'name': '', 'type': 'uint256'}], 'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': 'enode', 'type': 'address'}], 'name': 'lockDeposit', 'outputs': [],
        'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': True, 'inputs': [{'name': 'investor', 'type': 'address'}], 'name': 'freeBalanceOf',
        'outputs': [{'name': '', 'type': 'uint256'}], 'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [{'name': 'investor', 'type': 'address'}], 'name': 'lockedBalanceOf',
        'outputs': [{'name': '', 'type': 'uint256'}], 'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'totalFreeBalance', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': '_nextLockTime', 'type': 'uint256'}], 'name': 'setNextLockTime',
        'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'onlyNewLock', 'outputs': [], 'payable': False,
        'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': 'investor', 'type': 'address'}], 'name': 'settle', 'outputs': [],
        'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'nextRaiseTime', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [{'name': '', 'type': 'address'}], 'name': 'investors',
        'outputs': [{'name': 'freeBalance', 'type': 'uint256'}, {'name': 'lockedBalance', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': '_lockPeriod', 'type': 'uint256'}], 'name': 'setLockPeriod',
        'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': '_settlementPeriod', 'type': 'uint256'}], 'name': 'setSettlementPeriod',
        'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'newSettlement', 'outputs': [], 'payable': False,
        'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'newRaise', 'outputs': [], 'payable': False,
        'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': '_nextSettlementTime', 'type': 'uint256'}],
        'name': 'setNextSettlementTime', 'outputs': [], 'payable': False, 'stateMutability': 'nonpayable',
        'type': 'function'}, {'constant': False, 'inputs': [], 'name': 'settleAll', 'outputs': [], 'payable': False,
                              'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': '_nextRaiseTime', 'type': 'uint256'}], 'name': 'setNextRaiseTime',
        'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'deposit', 'outputs': [], 'payable': True,
        'stateMutability': 'payable', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'getEnodes', 'outputs': [{'name': '', 'type': 'address[]'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': True, 'inputs': [], 'name': 'enodeThreshold', 'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'constant': False, 'inputs': [], 'name': 'lockAllDeposit', 'outputs': [], 'payable': False,
        'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': False, 'inputs': [{'name': '_enodeThreshold', 'type': 'uint256'}], 'name': 'setEnodeThreshold',
        'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'},
       {'constant': True, 'inputs': [{'name': '', 'type': 'uint256'}], 'name': 'investments',
        'outputs': [{'name': '', 'type': 'uint256'}], 'payable': False, 'stateMutability': 'view', 'type': 'function'},
       {'inputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'constructor'},
       {'payable': True, 'stateMutability': 'payable', 'type': 'fallback'}, {'anonymous': False, 'inputs': [
        {'indexed': False, 'name': 'who', 'type': 'address'}, {'indexed': False, 'name': 'amount', 'type': 'uint256'},
        {'indexed': False, 'name': 'total', 'type': 'uint256'}], 'name': 'FundBonusPool', 'type': 'event'},
       {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'what', 'type': 'string'},
                                       {'indexed': False, 'name': 'value', 'type': 'uint256'}], 'name': 'SetConfig',
        'type': 'event'}, {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'what', 'type': 'string'},
                                                          {'indexed': False, 'name': 'when', 'type': 'uint256'}],
                           'name': 'SetTime', 'type': 'event'},
       {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'when', 'type': 'uint256'}], 'name': 'NewRaise',
        'type': 'event'}, {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'who', 'type': 'address'},
                                                          {'indexed': False, 'name': 'investment', 'type': 'uint256'}],
                           'name': 'NewEnode', 'type': 'event'}, {'anonymous': False, 'inputs': [
        {'indexed': False, 'name': 'who', 'type': 'address'}, {'indexed': False, 'name': 'balance', 'type': 'uint256'}],
                                                                  'name': 'EnodeQuit', 'type': 'event'},
       {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'who', 'type': 'address'},
                                       {'indexed': False, 'name': 'amount', 'type': 'uint256'},
                                       {'indexed': False, 'name': 'total', 'type': 'uint256'}], 'name': 'AddInvestment',
        'type': 'event'}, {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'who', 'type': 'address'},
                                                          {'indexed': False, 'name': 'amount', 'type': 'uint256'},
                                                          {'indexed': False, 'name': 'total', 'type': 'uint256'}],
                           'name': 'SubInvestment', 'type': 'event'},
       {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'when', 'type': 'uint256'}], 'name': 'NewLock',
        'type': 'event'},
       {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'when', 'type': 'uint256'}], 'name': 'NewSettlement',
        'type': 'event'}, {'anonymous': False, 'inputs': [{'indexed': False, 'name': 'who', 'type': 'address'},
                                                          {'indexed': False, 'name': 'income', 'type': 'uint256'}],
                           'name': 'ApplyForSettlement', 'type': 'event'}]

contract_address = cfg['reward']['address']

reward = cf.cpc.contract(abi=abi, address=contract_address)

#
#
# def set_period(reward, cf, owner, raise_p, lock_p, settle_p):
#     estimated_gas = reward.functions.setRaisePeriod(raise_p).estimateGas({"from": owner, "value": 0})
#     tx_hash = reward.functions.setRaisePeriod(raise_p).raw_transact({"from": owner, "value": 0, "gas": estimated_gas},
#                                                                     chainId=337,
#                                                                     keypath='tool/test_key', password='password')
#     tx_receipt = cf.cpc.waitForTransactionReceipt(tx_hash)
#     print("owner sets raise period, result: ", tx_receipt["status"])
#     estimated_gas = reward.functions.setLockPeriod(lock_p).estimateGas({"from": owner, "value": 0})
#     tx_hash = reward.functions.setLockPeriod(lock_p).raw_transact({"from": owner, "value": 0, "gas": estimated_gas},
#                                                                   chainId=337,
#                                                                   keypath='tool/test_key', password='password')
#     tx_receipt = cf.cpc.waitForTransactionReceipt(tx_hash)
#     print("owner sets lock period, result: ", tx_receipt["status"])
#     estimated_gas = reward.functions.setSettlementPeriod(settle_p).estimateGas({"from": owner, "value": 0})
#     tx_hash = reward.functions.setSettlementPeriod(settle_p).raw_transact(
#         {"from": owner, "value": 0, "gas": estimated_gas}, chainId=337,
#         keypath='tool/test_key', password='password')
#     tx_receipt = cf.cpc.waitForTransactionReceipt(tx_hash)
#     print("owner sets settlement period, result: ", tx_receipt["status"])
#

def sendRawTransaction(toAddr=None, value=10):
    with open('tool/test_key') as keyfile:
        encrypted_key = keyfile.read()
    from_addr = cf.toChecksumAddress('14ac5cf8f33696722386163be8a59e5f81e4157d')
    to_addr = cf.toChecksumAddress(toAddr)
    private_key_for_senders_account = cf.cpc.account.decrypt(encrypted_key, 'password')

    gas = cf.cpc.estimateGas({'to': to_addr, 'value': cf.toWei(value, 'ether')})

    tx_dict = dict(
        type=0,
        nonce=cf.cpc.getTransactionCount(from_addr),
        gasPrice=cf.cpc.gasPrice,
        gas=gas,
        to=to_addr,
        value=cf.toWei(value, 'ether'),
        # data='中文'.encode(),
        data=b'',
        chainId=337,
    )
    signed_txn = cf.cpc.account.signTransaction(tx_dict,
                                                private_key_for_senders_account,
                                                )

    print("sendRawTransaction:")
    tx_hash = cf.cpc.sendRawTransaction(signed_txn.rawTransaction)
    print(cf.toHex(tx_hash))


def new_raise():
    print(owner)
    tx_hash = reward.functions.newRaise().raw_transact(transaction={"from": owner, "value": 0, "gas": 10000000},
                                                       keypath='tool/test_key', password='password', chainId=337)
    tx_receipt = cf.cpc.waitForTransactionReceipt(tx_hash)
    print("owner starts a new raise, result: ", tx_receipt["status"])
    import os, sys
    sys.path.append('../../..')
    os.chdir(sys.path[0])
    from reward_test.config import cfg
    contract_address = cfg['reward']['address']
    print('contract address:', contract_address)

    sendRawTransaction(toAddr=contract_address)
    return tx_receipt["status"]


def new_lock():
    tx_hash = reward.functions.newLock().raw_transact(transaction={"from": owner, "value": 0, "gas": 10000000},
                                                      keypath='tool/test_key', password='password', chainId=337)
    tx_receipt = cf.cpc.waitForTransactionReceipt(tx_hash)
    print("owner starts a new lock and locks all free balance of enodes, result: ", tx_receipt["status"])
    return tx_receipt["status"]


def new_settlement():
    tx_hash = reward.functions.newSettlement().raw_transact(transaction={"from": owner, "value": 0, "gas": 10000000},
                                                            keypath='tool/test_key', password='password', chainId=337)
    tx_receipt = cf.cpc.waitForTransactionReceipt(tx_hash)
    print("owner starts a new settlement and distribute interest for all enodes, result: ", tx_receipt["status"])
    return tx_receipt['status']


def monitor():
    round = reward.functions.round().call()
    total_free_balance = reward.functions.totalFreeBalance().call()
    total_locked_balance = reward.functions.totalLockedBalance().call()
    bonus = reward.functions.bonus(round).call()
    investment = reward.functions.investments(round).call()
    in_raise = reward.functions.inRaise().call()
    in_lock = reward.functions.inLock().call()
    in_settlement = reward.functions.inSettlement().call()
    bonus_pool = reward.functions.bonusPool().call()
    raise_period = reward.functions.raisePeriod().call()
    lock_period = reward.functions.lockPeriod().call()
    settlement_period = reward.functions.settlementPeriod().call()
    enode_threshold = reward.functions.enodeThreshold().call()
    next_raise_time = reward.functions.nextRaiseTime().call()
    next_lock_time = reward.functions.nextLockTime().call()
    next_settlement_time = reward.functions.nextSettlementTime().call()
    enodes = reward.functions.getEnodes().call()

    print("*************all configs*********************")
    print("total free balance: ", cf.fromWei(total_free_balance, "ether"))
    print("total locked balance: ", cf.fromWei(total_locked_balance, "ether"))
    print("is in raise: ", in_raise)
    print("is in lock: ", in_lock)
    print("is in settlement: ", in_settlement)
    print("bonus pool: ", cf.fromWei(bonus_pool, "ether"))
    print("raise period: ", raise_period)
    print("lock period: ", lock_period)
    print("settlement period: ", settlement_period)
    print("enode threshold: ", cf.fromWei(enode_threshold, "ether"))
    print("round: ", round)
    print("bonus of this round: ", cf.fromWei(bonus, "ether"))
    print("investment of this round: ", cf.fromWei(investment, "ether"))
    print("next raise time: ", next_raise_time)
    print("next lock time: ", next_lock_time)
    print("next settlement time: ", next_settlement_time)
    print("number of enodes: ", len(enodes))
    print(enodes)
    print("*********************************************")


# def main():
# deploy_new_contract()
# set_period(reward, cf, owner, 60 * 60, 60 * 60, 60 * 60)
# test_case_1()
# compile_file()
# transfer_money()
# new_raise()
# new_lock()
# new_settlement()
# monitor()
# period = 60 * 15

# set_period(reward, cf, owner, period, period, period)


if __name__ == '__main__':
    # deploy_new_contract()
    # set_period(reward, cf, owner, 15 * 60, 15 * 60, 15 * 60)
    # new_raise()
    # new_lock()
    # new_settlement()
    monitor()
